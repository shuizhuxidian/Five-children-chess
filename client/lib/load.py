# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *
import util
from sound import play_sound
from image import *

class Load:
    
    
    def __init__(self, screen):
        self.screen = screen


    def run(self, elapse):
        self.draw()
        for e in pygame.event.get():
            if e.type == QUIT:
                return 'quit'
				
            elif e.type == VIDEORESIZE:
                SCREEN_SIZE = e.size
                width, height = self.screen.get_size()
                try:
					self.screen = pygame.display.set_mode(SCREEN_SIZE, HWSURFACE | SRCALPHA |RESIZABLE, 32)
                except:
					self.screen = pygame.display.set_mode((width, height), HWSURFACE | SRCALPHA |RESIZABLE, 32)
                refresh(self.screen)
				
            elif e.type == KEYDOWN:
                return 'menu'
            elif e.type == MOUSEBUTTONDOWN:
                return 'menu'
        return 'load'

    def draw(self):
        plot_background(self.screen,128)
        w_width, w_height = self.screen.get_size()
        pos_x = (w_width - 64*3)/2
        pos_y = (w_height - 64*2)/2
        util.myprint(self.screen, unicode('五子棋', 'utf-8'), (pos_x, pos_y), 'title',(255, 0, 0))
        pos_x = (w_width - 32*18/3 -32*4/2)/2
        pos_y = pos_y + 64
        util.myprint(self.screen, 'Five Children Chess', (pos_x, pos_y), 'english',(255, 0, 0))
        pos_x = (w_width - 32*16/3)/2
        pos_y = pos_y + pos_y/2
        util.myprint(self.screen, 'Press any key', (pos_x, pos_y), 'english')
		
