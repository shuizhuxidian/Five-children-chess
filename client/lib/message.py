# -*- coding: utf-8 -*-

from Tkinter import *
import tkMessageBox


class login_message:
    def __init__(self, HOST = '', PORT = '', nick = '', title = '提示框'):
        self.root = Tk()
        self.HOST = HOST
        self.PORT = PORT
        self.nick = nick
        #print self.HOST, self.PORT, self.nick        
        self.root.title(title)

        Label(self.root, text="IP：").grid(sticky=E)
        Label(self.root, text="端口：").grid(sticky=E)
        Label(self.root, text="昵称：").grid(sticky=E)

        self.e1 = Entry(self.root)
        str1 = StringVar()        
        self.e1['textvariable'] = str1
        str1.set(HOST)

        str2 = StringVar() 
        self.e2 = Entry(self.root)     
        self.e2['textvariable'] = str2
        str2.set(PORT)

        str3 = StringVar() 
        self.e3 = Entry(self.root)
        self.e3['textvariable'] = str3        
        str3.set(nick)
        

        self.e1.grid(row=0, column=1)
        self.e2.grid(row=1, column=1)
        self.e3.grid(row=2, column=1)

        button1 = Button(self.root, text='确定')
        button1.grid(row=4, column=1)
        button1['command'] = self.on_click
        
    def on_click(self):        
        #label['text'] = text.get()
        self.HOST = self.e1.get()
        self.PORT = self.e2.get()
        self.nick = self.e3.get()
        self.root.destroy()
        #print self.HOST, self.PORT, self.nick
                        
    def pop(self):
        mainloop()
        #print self.HOST, self.PORT, self.nick
        return self.HOST, self.PORT, self.nick


class message_box:
    def __init__(self, message = '', title = '提示框',  button_name = '确定'):
        self.root = Tk(className=title)
        Label(self.root, text=message).pack()
        button = Button(self.root, text=button_name)
        button.pack()
        #button.grid(sticky=S)
        button['command'] = self.on_click

    def on_click(self):
        self.root.destroy()

    def pop(self):       
        mainloop()

class ask_box:
    def __init__(self, message = '', title = '提示框',  button_yes = '确定', button_no = '取消'):
        self.root = Tk(className=title)
        Label(self.root, text=message).grid(sticky=W+E)
        button1 = Button(self.root, text=button_yes)
        #button1.pack()
        button1.grid(row=1, column=0,sticky=W)
        button1['command'] = self.yes_click_ask
        button2 = Button(self.root, text=button_no)
        #button2.pack()
        button2.grid(row=1, column=1,sticky=E)
        button2['command'] = self.no_click_ask
        

    def yes_click_ask(self):
        self.yesorno = 1
        self.root.destroy()

    def no_click_ask(self):
        self.yesorno = 0
        self.root.destroy()


    def pop(self):       
        mainloop()
        return self.yesorno



#tologo()
#message_box('11','11')
#print ask_box('11','骚年，再来一发？')
#a = login_message('加入游戏','11','22','33')
#a.login()
#a = message_box('11','22','33')
#a.message()
#a = ask_box('11','22','33','44')
#print a.pop()
