# -*- coding:utf-8 -*- 


import pygame
from pygame.locals import *


from menu import Menu
from load import Load
from about import About
from chess import Chess
import sound, util, image

class Game:
    def __init__(self):
        self.stat = 'load'
        # init pygame
        pygame.mixer.pre_init(44100, 16, 2, 1024*4)
        pygame.init()
        pygame.display.set_caption('逐鹿中原V0')
        
        try:
            self.screen = pygame.display.set_mode((640, 480), 
                    HWSURFACE | SRCALPHA |RESIZABLE, 32)
        except:
            self.screen = pygame.display.set_mode((640, 480), 
                    SRCALPHA|RESIZABLE, 32)
        try:
            pygame.display.set_icon(pygame.image.load(
                util.file_path('icon.png')).convert_alpha())
        except:
            # some platfom do not allow change icon after shown
            pass

        self.init()
        # init sub modules

        self.load = Load (self.screen)
        self.menu = Menu (self.screen)   # menu show start menu
        #self.teach= Teach(self.screen)
        self.chess = Chess (self.screen)
        self.about= About(self.screen)
        

        

    def init(self):
        util.init()
        sound.load()
        image.load(self.screen)
        

    def loop(self):
        clock = pygame.time.Clock()
        while self.stat != 'quit':
            elapse = clock.tick(25)
            if self.stat == 'load':
                self.stat = self.load.run(elapse)
            elif self.stat == 'menu':
                self.chess.empty_list()
                self.stat = self.menu.run(elapse)
            elif self.stat == 'about':
                self.stat = self.about.run(elapse)#!!!!!
            elif self.stat == 'chess':
                self.stat = self.chess.run(elapse)

            pygame.display.update()
        pygame.quit()

def run():
    game = Game()
    game.loop()

if __name__ == '__main__':
    run()
