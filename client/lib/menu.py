# -*- coding:utf-8 -*- 

import pygame
from pygame.locals import *
from util import myprint
from sound import play_sound
from image import *

pos_x = 0
pos_y = 0
w_width = 0
w_height = 0

class Menu:
    OPTS = [
            #'teach',
            'chess',
            #'change',
            'about',
            'quit'
        ]
    OPTS_Ch = [
            #'教  程',
            '加入游戏',
            #'设  置',
            '关        于',
            '退        出'
        ]
    def __init__(self, screen):
        global pos_x,pos_y,w_width, w_height
        self.screen = screen
        self.current = 0
        w_width, w_height = self.screen.get_size()
        pos_x = (w_width - 36*4)/2
        pos_y = (w_height - 36*len(self.OPTS_Ch)*2 + 36)/2
		
		
    def run(self, elapse):
        global pos_x,pos_y
        self.draw()
        for e in pygame.event.get():
            if e.type == QUIT:
                return 'quit'
				
            elif e.type == VIDEORESIZE:
                SCREEN_SIZE = e.size
                width, height = self.screen.get_size()
                try:
					self.screen = pygame.display.set_mode(SCREEN_SIZE, HWSURFACE | SRCALPHA |RESIZABLE, 32)
                except:
					self.screen = pygame.display.set_mode((width, height), HWSURFACE | SRCALPHA |RESIZABLE, 32)
                refresh(self.screen)
				
            elif e.type == KEYDOWN:
                if e.key == K_UP:
                    self.current = (self.current - 1) % len(self.OPTS)
                    play_sound('menu')
                elif e.key == K_DOWN:
                    self.current = (self.current + 1) % len(self.OPTS)
                    play_sound('menu')
                elif e.key == K_RETURN:
                    return self.OPTS[self.current].lower()
                elif e.key == K_ESCAPE:
                    return 'load'
                    #return 'about'
            elif e.type == MOUSEMOTION:
                (x, y) = e.pos 
                current = int((y - pos_y) /36)
                if (current >= 0) and (current%2 == 0):
                    current = current/2
                    if self.current != current:
                        play_sound('menu')
                    self.current = current
                    #print current/2
            elif e.type == MOUSEBUTTONDOWN:
                if e.button == 1:
                    try:
                        return self.OPTS[self.current].lower()
                    except:
                        pass
                elif e.button == 3:
                    return 'load'
			
        return 'menu'

    def draw(self):
        global pos_x,pos_y,w_width, w_height
        w_width, w_height = self.screen.get_size()
        pos_x = (w_width - 36*4)/2
        pos_y = (w_height - 36*len(self.OPTS_Ch)*2 + 36)/2
        #plot_background(self.screen,128)
        plot_background(self.screen)
        for idx in xrange(len(self.OPTS_Ch)):
            if idx == self.current:
                myprint(self.screen, unicode(self.OPTS_Ch[idx], 'utf-8'), (pos_x, 36 * 2 * idx + pos_y),'m')
            else:
                myprint(self.screen, unicode(self.OPTS_Ch[idx], 'utf-8'), (pos_x, 36 * 2 * idx + pos_y),'m',
                        (160, 160, 160))

