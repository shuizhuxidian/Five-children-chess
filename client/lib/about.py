# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *
from util import myprint
from util import mysize
from sound import play_sound
from image import *

class About:
    
    
    def __init__(self, screen):
        self.screen = screen

    def run(self, elapse):
        self.draw()
        for e in pygame.event.get():
            if e.type == QUIT:
                return 'quit'
            elif e.type == VIDEORESIZE:
                SCREEN_SIZE = e.size
                width, height = self.screen.get_size()
                try:
		    self.screen = pygame.display.set_mode(SCREEN_SIZE, HWSURFACE | SRCALPHA |RESIZABLE, 32)
                except:
		    self.screen = pygame.display.set_mode((width, height), HWSURFACE | SRCALPHA |RESIZABLE, 32)
                refresh(self.screen)
            elif e.type == KEYDOWN:
                return 'menu'
            elif e.type == MOUSEBUTTONDOWN:
                return 'menu'	
				
        return 'about'

    def draw(self):
        w_width, w_height = self.screen.get_size()
        self.screen.fill((0,0,0))        
        title = 'This game is created in 2015 by '
        t_width, t_height =  mysize(title,'english')
        describe1 = 'BGM:十面埋伏'
        describe2 = 'Version number:0.5'
        d_width1, d_height1 =  mysize(describe1)
        d_width2, d_height2 =  mysize(describe2)
        size_w, size_h = transform_logo(self.screen, 2.2)
        
        t_pos_x = int((w_width - (t_width + size_w/2))/2)
        t_pos_y = int((w_height - t_height - size_h - d_height1 - d_height2)/2)
        myprint(self.screen, title, (t_pos_x, t_pos_y),'english')
		
        l_pos_x = int(t_pos_x + t_width - size_w/2)
        l_pos_y = int(t_pos_y + t_height)
        plot_logo(self.screen,(l_pos_x,l_pos_y))
		
        d_pos_x1 = int(l_pos_x - (d_width1 - size_w)/2)
        d_pos_y1 = int(l_pos_y + size_h) + d_height1
        #myprint(self.screen, unicode(describe1, 'utf-8'), (d_pos_x1, d_pos_y1),'s')
        myprint(self.screen, unicode(describe1, 'utf-8'), (l_pos_x, d_pos_y1),'s')
        d_pos_x2 = int(d_pos_x1 - (d_width2 - d_width1)/2)
        d_pos_y2 = int(d_pos_y1 + d_height1)
        #print size_w, d_width1, d_height1
        #myprint(self.screen, unicode(describe2, 'utf-8'), (d_pos_x2, d_pos_y2),'s')
        myprint(self.screen, unicode(describe2, 'utf-8'), (l_pos_x, d_pos_y2),'s')
		
		
        myprint(self.screen, unicode('BG:溪亭对弈图 by:唐寅', 'utf-8'), (l_pos_x, l_pos_y+ size_h),'s')
		
		
