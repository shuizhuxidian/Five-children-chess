import pygame
import util

background_bak = ''
background = ''
logo_bak = ''
logo = ''

def load(screen):
    global background_bak, background, logo_bak, logo
    background_bak = pygame.image.load(util.file_path("background.png")).convert()
    width, height = screen.get_size()
    background = pygame.transform.scale(background_bak, (width, height))
    #background.set_masks(0, 128, 128, 128)
    logo_bak = pygame.image.load(util.file_path("logo.png")).convert_alpha()
    logo = logo_bak
	
def refresh(screen):
	global background_bak, background
	w_w, w_h = screen.get_size()
	background = pygame.transform.scale(background_bak, (w_w, w_h))
	
	
def plot_background(screen, alpha = 255):
	global background
	background.set_alpha(alpha)
	screen.fill((0,0,0))
	screen.blit(background, (0, 0))
	
def transform_logo(screen,scale = 3):
	global logo_bak, logo
	w_w, w_h = screen.get_size()
	logo_w, logo_h = logo.get_size()
	change_h = w_h/scale
	change_w = logo_w*change_h/logo_h
	change = (int(change_w), int(change_h))
	logo = pygame.transform.scale(logo_bak, change)
	return int(change_w),int(change_h)
	
def plot_logo(screen, pos):
	global logo
	screen.blit(logo, pos)
	